/**
 * Aaron Presley
 * April, 2015
 *
 * This script is intended to provide various helpers and tools
 * for working with tables on Inavero projects.
 *
 */

var $ = require('jquery');

var TableManager = function(elemSelector, options){
    // The main table-container of this specific table
    this.tableContainerElem = $(elemSelector);

    // The column number of the initially visible column when
    // the user is on mobile
    this.initVisibleMobileColNum = null;


    // Will hold the total number of columns we have in this table
    this.totalCols = null;

    // Determines whether or not we enable the mobile logic
    this.mobileEnabled = false;

    this.__setOptions(options);

    // This will store the currently shown col number
    this.currMobileVisibleColNum = this.initVisibleMobileColNum;

    // Attempt to figure out how many cols this table has if it wasn't
    // passed in the options
    if( this.totalCols == null ) {
        this.totalCols = this.tableContainerElem.find('table > tbody > ' +
            'tr:first-child > td').length;
    }

    if( this.mobileEnabled ) {
        this.__prepForMobile();
        this.__initMobileActions();
    }
};

/**
 * Takes the passed options object and sets
 * @param options
 * @private
 */
TableManager.prototype.__setOptions = function(options){
    for( var field in options ){
        // Don't set this field if it hasn't been defined in the constructor
        if( !this.hasOwnProperty(field) ) {
            console.warn("The \"" + field + "\" option was ignored because " +
                "it wasn't declared in the TableManager constructor.");
            continue;
        }
        this[field] = options[field];
    }
};

/**
 * Returns a list of jQuery objects for the given column
 * @param colNum
 * @returns {*}
 * @private
 */
TableManager.prototype.__getColumnCells = function(colNum){
    var tableSections = ['thead', 'tbody', 'tfoot'];
    var cellTypes = ['th', 'td'];

    // Ensuring column numbers aren't
    // zero-based
    colNum++;

    // Starting out with an empty selector
    var selector = '';

    // Dynamically generate the selector for all pieces of the table,
    // as well as for each type of cell.
    for( var x in tableSections ){
        for( var y in cellTypes ){
            var section = tableSections[x];
            var cellType = cellTypes[y];
            selector += 'table > ' + section + ' > tr > ' + cellType +
                ':nth-child(' + colNum + '), ';
        }
    }

    // Removing the last instance of the selector
    selector = selector.replace(/, $/, '');

    // Getting all of the matching jQuery objects with the given
    // selector
    return this.tableContainerElem.find(selector);
};

/**
 * Various actions to be taken in order to prepare the front-end
 * to be seen on a mobile
 * @private
 */
TableManager.prototype.__prepForMobile = function(){
    // Setting the initial table column to be visible
    var visibleCols = this.__getColumnCells(this.initVisibleMobileColNum);
    visibleCols.removeClass('hidden-xs');
};

/**
 * Adds a highlighted class to all cells in this table at that
 * column's position
 * @param colNum
 * @param onComplete
 */
TableManager.prototype.setHighlightedCol = function(colNum, onComplete){
    var newCols = this.__getColumnCells(colNum);

    // Bail out if we couldn't find anything to match that selector
    if( newCols.length == 0 ) {
        console.error("This table doesn't have that many columns");
        return false;
    }

    // Removing all currently highlighted columns
    var prevCols = this.clearHighlightedCol();

    // Adding the highlighted class
    newCols.addClass('th-highlighted');

    // Pass the newly highlighted jquery objects to the user method
    // of it exists
    if( onComplete != undefined )
        onComplete(prevCols, newCols);
};

/**
 * Removes all of this table's highlighted columns
 */
TableManager.prototype.clearHighlightedCol = function(){
    return this.tableContainerElem.find('.td-highlighted, .th-highlighted')
        .removeClass('th-highlighted')
        .removeClass('td-highlighted');
};

TableManager.prototype.__setMobileVisibleCol = function(colNum){
    // Getting our Curr
    var currColNum = this.currMobileVisibleColNum;
    var nextColNum = colNum;

    // Never showing anything lower than col 1
    if( nextColNum < 0 )
        nextColNum = 0;

    // Never showing more than total cols
    else if( nextColNum > this.totalCols )
        nextColNum = this.totalCols;

    var currVisibleCols = this.__getColumnCells(currColNum);
    currVisibleCols.addClass('hidden-xs');

    var nextVisibleCols = this.__getColumnCells(nextColNum);
    nextVisibleCols.removeClass('hidden-xs');

    this.currMobileVisibleColNum = nextColNum;
};

TableManager.prototype.showPrevColumn = function(){
    this.__setMobileVisibleCol(this.currMobileVisibleColNum-1);
};

TableManager.prototype.showNextColumn = function(){
    this.__setMobileVisibleCol(this.currMobileVisibleColNum+1);
};

TableManager.prototype.__initMobileActions = function(){
    var self = this;

    // When the user clicks on a LEFT arrow
    this.tableContainerElem.find('a.col-move-left').click(function(e){
        e.preventDefault();
        self.showPrevColumn();
    });

    // When the user clicks on a RIGHT arrow
    this.tableContainerElem.find('a.col-move-right').click(function(e){
        e.preventDefault();
        self.showNextColumn();
    });
};

module.exports = TableManager;

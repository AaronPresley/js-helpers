var $ = require('jquery');
var request = require('request');

/**
 * A class made specifically to allow for generic Django forms to
 * be submitted via ajax and properly display any errors under their
 * correct field.
 *
 * Note: This assume the form is following Bootstrap standards. It's
 * also very specific to how I personally organize my Django forms and
 * might not work for you out of the gate.
 *
 * @param formElemSelector {string} - the CSS selector for this specific form
 * @param options {object} - Any additional options that should be passed
 *
 * @constructor
 */
var DjangoForm = function(formElemSelector, options){
    // The main form element
    this.elem = $(formElemSelector);

    // Make sure the form exists
    if( !this.elem.length )
        throw "The element \"" + formElemSelector + "\" doesn't " +
            "exist on this page";

    // The container for our error message
    this.flashMessageElem = this.elem.find('.flash-message');

    // What will happen when the form has been
    // successfully submitted
    this.onSuccess = null;

    // The default settings for updating the submit button
    // text when the form is submitting
    this.buttonLoadingText = null;
    this.buttonOriginalText = null;

    // Processing any options that were passed
    this.setOptions(options);

    this.action();
};

/**
 * Reads all optiosn passed to the construct and assigns them
 * to the proper object properties. Also allows to reset options later
 * in the object's lifecycle.
 *
 * @param options {object} - The options for this project
 */
DjangoForm.prototype.setOptions = function(options){
    if( options == undefined )
        return false;

    if( options['onSuccess'] != undefined )
        this.onSuccess = options['onSuccess'];
    if( options['buttonLoadingText'] != undefined )
        this.buttonLoadingText = options['buttonLoadingText'];
};

/**
 * Gathers all of the form's data into something to be posted
 */
DjangoForm.prototype.gatherData = function(){
    var data = {};
    var dataArray = this.elem.serializeArray();

    for( var x in dataArray ) {
        var field = dataArray[x]['name'];
        var value = dataArray[x]['value'];
        data[field] = value;
    }

    return data;
};

/**
 * Loops through all errors from a Django server response and assigns
 * them to the overall form or to a single field
 *
 * @param errorObj {object} - The error object from the Django response
 */
DjangoForm.prototype.processErrorResponse = function(errorObj){
    // Loop through each error
    for( var x in errorObj ) {
        // This is an overall form error
        if( x == "__all__" )
            this.setFormMessage(errorObj[x]);

        // This is an error for a single field
        else
            this.setFieldMessage(x, errorObj[x]);
    }
};

/**
 * Removes all existing error outputs from this form
 */
DjangoForm.prototype.clearFormMessages = function(){
    this.elem.find('.flash-field-message').remove();
    this.flashMessageElem.html('');
};

/**
 * Sets a Bootstrap alert div at the top of a form.
 *
 * @param messages {string|array} - The message to display at the top of this form
 * @param status {string} - The bootstrap class to set in the alert div
 * @param doDisappear {bool} - Whether or not to hide the message after a time
 */
DjangoForm.prototype.setFormMessage = function(messages, status, doDisappear){
    // Assuming the worst
    status = status == undefined ? 'danger' : status;

    var messageString = "";

    // This is a simple string
    if( typeof messages == "string" )
        messageString = messages;

    // We were given an array so we need to concatenate into a string with
    // a simple "- " at the beginning of each line
    else {
        for( var x in messages )
            messageString += "- " + messages[x];
    }

    // Assigning the alert html to our flash message container
    this.flashMessageElem.html('<div class="alert alert-' + status + '">' +
        messageString + '</div>');

    // We were told to hide the message, so set a timer to remove it
    if( doDisappear ){
        setTimeout(function(){
            this.flashMessageElem.find('> div').fadeOut('fast', function(){
                $(this).remove();
            });
        }.bind(this), 6000);
    }
};

/**
 * Sets a message for a single field
 *
 * @param field {string} - The name of the field
 * @param messages {string|array} - The message to display
 * @param status {string} - The bootstrap class to set in the text span
 */
DjangoForm.prototype.setFieldMessage = function(field, messages, status){
    // Assuming the worst
    status = status == undefined ? 'danger' : status;

    var messageString = "";

    // Finding the field and its container
    var elem = this.elem.find('*[name='+field+']');
    var elemGroup = elem.closest('.form-group');

    // This is a simple string
    if( typeof messages == "string" )
        messageString = messages;

    // We were given an array so we need to concatenate into a string with
    // a simple "- " at the beginning of each line
    else {
        for( var x in messages )
            messageString += "- " + messages[x];
    }

    // Removing any existing errors
    elemGroup.find('.flash-field-message').remove();

    // Adding this error
    elem.after('<span class="flash-field-message text-'+status+'">' +
        messageString + '</span>');
};

/**
 * Allows for setting the submit button to busy or not
 *
 * @param isBusy {bool} - Whether or not it should be marked as busy
 */
DjangoForm.prototype.submitButtonIsBusy = function(isBusy){
    // Finding our submit button
    var submitButton = this.elem.find('button[type="submit"]');

    // If we want to update the button text, get the
    // original text now
    if( !this.buttonOriginalText )
        this.buttonOriginalText = submitButton.html();

    // We're setting it to busy
    if( isBusy ){
        submitButton.addClass('disabled').prop('disabled', true);
        if( this.buttonLoadingText )
            submitButton.html(this.buttonLoadingText);

    // We're removing the busy status
    } else {
        submitButton.removeClass('disabled').prop('disabled', false);
        if( this.buttonLoadingText ) {
            submitButton.html(this.buttonOriginalText);
            this.buttonOriginalText = null;
        }
    }
};

/**
 * Performs the ajax request to whatever the action attribute is set to
 */
DjangoForm.prototype.send = function(){
    this.clearFormMessages();
    this.submitButtonIsBusy(true);

    var options = {
        uri:        this.elem.attr('action'),
        method:     'POST',
        json:       this.gatherData()
    };

    request(options, function (error, response, body) {
        var bodyJson = JSON.parse(body);

        if( !error && response.statusCode == 200 ){
            // Resetting the form values and focused element
            this.elem.find('input, select, textarea').val('');
            this.elem.find('*:focus').blur();

            // Does the onSuccess option have a value?
            if( this.onSuccess ){
                this.onSuccess(this.elem);
                return true;
            }

            this.setFormMessage(resp['success'], "success", true);

        } else {
            this.processErrorResponse(bodyJson);
        }

        this.submitButtonIsBusy(false);
    });
};

/**
 * Initializes the listeners for this form
 */
DjangoForm.prototype.action = function(){
    var self = this;
    this.elem.submit(function(e){
        e.preventDefault();
        self.send();
    });
};

module.exports = DjangoForm;

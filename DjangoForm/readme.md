# Django Form

This is an object that makes it easy for my Django
forms to post their data via ajax, and output any
validation errors without ever having to reload
the page.

*NOTE:* This object assumes you're using Bootstrap
standards for your form's HTML.

## The Form

    <form id="contactForm">
        <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control" />
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

## Instantiate

    var DjangoForm = require('./djangoForm');
    var thisForm = new DjangoForm('#contactForm' {
        buttonLoadingText: 'Loading...',
        onSuccess: function(formElem) {
            console.log('I submitted!');
        }
    });

/**
 * An object to quickly and easily set, update, or remove cookies
 * in a browser. All methods are static for simplicity sake.
 *
 * @constructor
 */
function CookieManager(){}

/**
 * Sets a cookie value that expires at the provided date, or in
 * 2020 by default
 *
 * @param key {string} - They cookie's name
 * @param value {string} - The value to assign this cookie
 * @param expirationDate {date} - The date this cookie should expire
 */
CookieManager.set = function(key, value, expirationDate){
    // Expire this cookie way in the future by default
    expirationDate = expirationDate == undefined ? new Date(2020, 1, 1) : expirationDate;

    // Our key can't have any spaces in it
    var cookie_name = key.replace(/\s/g, "_");

    // Setting the cookie
    document.cookie = cookie_name + "="+ value +"; expires=" + expirationDate.toUTCString() + "; path=/;";
};


/**
 * Returns the value of the passed key
 *
 * @param key {string} - the name of the cookie to return
 */
CookieManager.get = function(key){
    // Splitting up our cookies
    var cookies = document.cookie.split(';');

    // Looping through each cookie
    for( var x in cookies ){
        // Getting the cookie string in the form of
        // cookie_name=value
        var cookie_data = cookies[x].split('=');

        // Is this the cookie we're looking for?
        if( key.trim() == cookie_data[0].trim() ) {
            // IE9 explodes here if you try to trim() a null or undefined
            var theCookie = cookie_data[1] ? cookie_data[1].trim() : cookie_data[1];

            if( theCookie == '' )
                return null;
            return theCookie;
        }
    }

    // The cookie wasn't found
    return null;
};

/**
 * Removes the passed cookie
 *
 * @param key {string} - the name of the cookie to remove
 */
CookieManager.delete = function(key){
    this.set(key, '');
};

module.exports = CookieManager;

/**
 * Aaron Presley
 * April, 2015
 *
 * This script is intended to provide various helpers and tools
 * for working with forms on Inavero projects.
 *
 */

var $ = require('jquery');


var FormManager = function(selector, options){
    // The form that we're wanting to monitor
    this.formElem = $(selector);
    if( this.formElem == 0 )
        throw "That element couldn't be found";

    // -- PUBLIC PROPERTIES --

    // Holds the percent of the fields that
    // have been filled
    this.percentComplete = 0;

    // Will hold all of this form's fields
    // and their current value;
    this.fields = {};

    // -- PRIVATE PROPERTIES --

    // How long we wait after a user has stopped typing
    // to run the __fieldWasChanged method
    this.__changeDelay = 500;

    // This function is run when the form's progress
    // is changed
    this.__userOnChange = null;

    // This is the user-defined function that
    // runs after the form is ready
    this.__userOnReady = null;

    // The user-defined function to run when the
    // form is submitted
    this.__userOnSubmit = null;

    // All flash messages will go in this container
    this.__flashContainer = this.formElem.find('.flash-message');

    // Setting the options
    this.__setOptions(options);

    // Initiating our listeners
    this.__actions();

    // Telling us we're ready
    this.__ready();
};

/**
 * Accepts the name of a field and returns its value, or
 * undefined if it has not been set
 *
 * @param fieldName {string} - The field name we're looking up
 */
FormManager.prototype.getFieldValue = function(fieldName){
    // Our default value
    var theVal = undefined;

    // If we were passed a string, we assume it's the name
    // of the field we want
    var fieldElem = this.formElem.find('*[name='+fieldName+']');

    // All of the input types that require us to look
    // for a "checked" value
    var checkedSelectorString = 'input[type=radio], input[type=checkbox]';

    // Does this field matched the checked selector?
    if( fieldElem.is(checkedSelectorString) ){
        // Loop through each element
        fieldElem.each(function(x){
            if( $(this).is(':checked') )
                theVal = $(this).val();
        });

    // This is just a normal field
    } else {
        theVal = fieldElem.val();
    }

    return theVal == "" ? undefined : theVal;
};

/**
 * Checks a form field for any errors, and returns
 * true / false
 *
 * @param fieldElem
 */
FormManager.prototype.fieldIsValid = function(fieldElem){
    var fieldVal = this.getFieldValue(fieldElem.attr('name'));

    if( !fieldElem.hasClass('optional') ){
        if( fieldVal == undefined )
            return false;
    }

    return true;
};

/**
 * Displays a bootstrap alert with the passed message. If no target is passed
 * it will look for the default target of an element with a class of "flash-message"
 *
 * @param message	String of the alert message
 * @param level		Can be "danger", "info", "warning"
 * @param target	The flash container you want to display the alert in
 */
FormManager.prototype.flashMessage = function(message, level, target){

    if( target == undefined ) {
        // Use our default flash target if one wasn't passed
        target = this.__flashContainer;

    } else {
        // We're wanting to generate the error in a specific element
        target = $(target);
    }

    // Ensure the target exists
    if( !target.length )
        throw "A flash message container couldn't be found.";

    // Setting the default level
    level = level == undefined ? "danger" : level;

    // Generate the alert and set the container
    var messageHtml = '<div class="alert alert-'+level+'">'+message+'</div>';
    target.html(messageHtml);

    // Scroll to the element
    $('html, body').animate({
        scrollTop: target.offset().top - 20
    }, 200);
};

FormManager.prototype.clearFlashMessage = function(target){
    target = target == undefined ? $('.flash-message') : $(target);
    target.html('');
};

/**
 * Allows the user to overwrite a handful of this objects
 * default values
 * @param options
 * @private
 */
FormManager.prototype.__setOptions = function(options){
    if( options == undefined )
        return;

    if( options['onChange'] != undefined )
        this.__userOnChange = options['onChange'];
    if( options['onReady'] != undefined )
        this.__userOnReady = options['onReady'];
    if( options['onSubmit'] != undefined )
        this.__userOnSubmit = options['onSubmit'];
};

/**
 * All actions that should be taken after a field in this
 * form has been changed
 *
 * @param fieldElem
 * @private
 */
FormManager.prototype.__fieldWasChanged = function(fieldElem){

    // Updating our value
    this.fields[fieldElem.attr('name')] = this.getFieldValue(fieldElem.attr('name'));

    // Calculate the completeness
    this.__calculatePercentageComplete();

    // Call the user's method
    if( this.__userOnChange )
        this.__userOnChange(this);
};

/**
 * Looks at all of this form's fields and determines what percentage
 * are valid.
 *
 * @private
 */
FormManager.prototype.__calculatePercentageComplete = function(){
    var totalValidFields = 0;

    for( var x in this.fields ){
        var thisField = this.formElem.find('*[name='+ x +']');
        if( this.fieldIsValid(thisField) )
            totalValidFields++;
    }

    this.percentComplete = totalValidFields / Object.keys(this.fields).length;
    this.percentComplete = this.percentComplete * 100;
    this.percentComplete = this.percentComplete.toFixed(1);
};

/**
 * Loops through all of our current fields and sets their
 * value to our field property
 *
 * @private
 */
FormManager.prototype.__collectFieldValues = function(){
    var self = this;

    // Resetting the current fields
    self.fields = {};

    var allFieldsSelector = 'input, textarea, select';
    self.formElem.find(allFieldsSelector).each(function(){
        var thisName = $(this).attr('name');
        self.fields[thisName] = self.getFieldValue(thisName);
    });
};

/**
 * All methods involving the page's event listers
 *
 * @private
 */
FormManager.prototype.__actions = function(){
    var self = this;
    this.__onChangeActions();

    // When the form is submitted
    this.formElem.submit(function(e){

        // Behave normally if the user hasn't set a custom
        // submit handler
        if( self.__userOnSubmit ) {
            e.preventDefault();
            return self.__userOnSubmit(self, e);
        }

    });
};

/**
 * All vars and methods associated with created the
 * OnChange event listener
 *
 * @private
 */
FormManager.prototype.__onChangeActions = function(){
    var self = this;

    // For a shorter variable to ref
    var textFieldSelector = 'input[type="text"], textarea';
    var choiceFieldSelector = 'input[type="radio"], input[type="checkbox"], select';

    // Our timeout obj
    var changeTimeout = null;

    // Our event listener for any fields that involve
    // the user typing / updating text
    this.formElem.find(textFieldSelector).keyup(function(){
        var thisField = $(this);

        // Run this after the user has paused for input
        var notifyWasChanged = function(){
            self.__fieldWasChanged( thisField );
        };

        // Kill our previous timeout object
        clearTimeout(changeTimeout);

        // Create the new timeout object
        changeTimeout = setTimeout(notifyWasChanged, self.__changeDelay);
    });

    // Our event listener for the choice fields
    this.formElem.find(choiceFieldSelector).change(function(){
        // Run the function without a delay
        self.__fieldWasChanged($(this));
    });
};

/**
 * All actions associated with marking this form object
 * as ready
 *
 * @private
 */
FormManager.prototype.__ready = function(){
    this.__collectFieldValues();
    this.__calculatePercentageComplete();

    // Run the user's method if it was passed
    if( this.__userOnReady )
        this.__userOnReady(this);
};

module.exports = FormManager;

# Form Manager Documentation

## Abstract

The Form Manager module was written to be used as a generic helper for working with
and managing forms.

## Initialize

	var fm = new FormManager('form#theForm', {
		// Options Here
	})

### Options

Below are all of the options that can be passed in the second parameter of the
constructor method.

| Arg Name				| Arg Type				| Description
|--						|--						|--
| onReady				| func(fmObj)			| Runs once the form has been prepped and is in a ready state.
| onChange				| func(fmObj)			| Is run every time a field is changed
| onSubmit				| func(fmObj, event)	| If passed, overwrites the default submit functionality


## Useful Variables

These FormManager properties can be referenced once the module has been
prepped. They can come in handy when doing page-specific logic.

| Variable				| Description
|--						|--
| formElem				| This is the jquery object of the form element.
| fields  				| An object containing all of this form's current fields and values (ex: `field['field_name']`)
| percentComplete		| Returns the percentage of valid fields in the passed form

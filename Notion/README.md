# Fixed Container Documentation

## Abstract

TODO 

## Initialize

### HTML

	<a href="#" class="show-notion" data-notion="#notionContentSelector">Info</a>
	<div id="notionContentSelector" style="display:none;">
		Content in here will show up in the notion
	</div>

### Javascript

	new Notion($('a.show-notion'));

### Options

Below are all of the options that can be passed in the second parameter of the
constructor method.

| Arg Name				| Arg Type				| Description
|--						|--						|--
| TODO					| TODO					| TODO
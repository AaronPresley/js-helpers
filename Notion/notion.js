var $ = require('jquery');

var Notion = function(triggerElem, options){
    var self = this;

    self.__options = options;

    // Append the notion message to the body by default
    self.appendElem = $('html');

    // The property that will hold our currently-shown notion
    self.currNotionElem = null;

    // What will hold our timeout for hiding the Notion
    self.hideNotionTimeout = null;

    // Setting up our vars
    self.triggerElem = triggerElem;
    self.contentTargetElem = $(self.triggerElem.data('notion'));

    self.notionOffset = self.triggerElem.data('notionOffset');
    if( self.notionOffset )
        self.notionOffset = self.notionOffset * 1;

    // We couldn't find the content that should go into
    // this notion
    if( self.contentTargetElem.length == 0 ){
        throw "Couldn't find the element \"" + self.triggerElem.data('notion') + "\"";
    }

    // Initialize the actions
    self.__initActions();
};

/**
 * Does all calculations regarding where to put our Notion element on the
 * page, relative to the current top-left position of the page
 *
 * @param location
 * @returns {{top: number, left: number}}
 */
Notion.prototype.setNotionPosition = function(){
    var self = this;
    var location = null;

    var top = 0;
    var left = 0;

    if( this.__options['location'] != undefined )
        location = this.__options['location'];

    // Get the top-left of the trigger element
    var triggerOffset = self.triggerElem.offset();

    switch( location ){

        case 'top-center':
            top = triggerOffset.top - self.currNotionElem.outerHeight() - 10;

            left = triggerOffset.left + (self.triggerElem.outerWidth() / 2) + 5;
            left -= self.currNotionElem.outerWidth() / 2;

            break;

        default:
            // We want our notion to be offset from our trigger. Keeping
            // in mind that there's a "tag" above the main Notion element
            top = triggerOffset.top + self.triggerElem.outerHeight() + 5;

            // Accounting for any padding in our notion elem
            top += self.currNotionElem.css('padding-top').replace('px', '') * 1;

            // Aligning the right border of our Notion to the left border
            // of our trigger element
            left = triggerOffset.left - self.currNotionElem.outerWidth();

            // Accounting for the distance from the right Notion border to
            // the center of the Notion's tag
            left += 45;


            break;
    }

    // Accounting for an offset present in the trigger elem
    if( self.notionOffset != undefined )
        left += self.notionOffset;

    self.currNotionElem.css({
        top: top,
        left: left
    });
};

/**
 * All actions required when we want to show a message over
 * our trigger
 */
Notion.prototype.showMessage = function(){
    var self = this;

    // Getting our HTML to show in case it has changed
    var content = self.contentTargetElem.html();

    // Are we doing anything other than the default position?
    var location = this.__options['location'];

    var notionHtml = '' +
        '<div id="notion" class="'+ (location != undefined ? location : '') +'">' +
            content +
        '</div>';

    // Create a jQuery object with this HTML
    self.currNotionElem = $(notionHtml);

    // Add the HTML and fade it in
    self.appendElem.append(self.currNotionElem);
    self.setNotionPosition();
    self.currNotionElem.css({
        opacity: 0,
        visibility:'visible'
    });
    self.currNotionElem.stop().animate({
        opacity: 1
    }, 200);
};

/**
 * All actions when we want to hide the Notion from the screen
 */
Notion.prototype.hideMessage = function(force_now){
    var self = this;

    force_now = force_now != undefined;

    var doHide = function() {
        $('#notion').remove();
        self.currNotionElem = null;
        self.hideNotionTimeout = null;
    };

    if( !force_now ) {
        self.hideNotionTimeout = setTimeout(function () {
            doHide();
        }, 200);
    } else {
        doHide();
    }
};

/**
 * Handles all actions regarding this trigger or any other elements
 * regarding this notion
 */
Notion.prototype.__initActions = function(){
    var self = this;

    self.triggerElem.hover(function(){
        self.showMessage();
    }, function(){
        self.hideMessage();
    });
};

module.exports = Notion;
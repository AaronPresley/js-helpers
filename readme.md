# Javascript Helpers

A collection of Javascript classes and scripts that I use in nearly
every project I create.

- [Cookie Manager](https://bitbucket.org/AaronPresley/js-helpers/src/master/CookieManager/) - A script to easily add, edit and delete cookies.
- [Django Form](https://bitbucket.org/AaronPresley/js-helpers/src/master/DjangoForm/) - A simple way to make your Django forms submit via ajax
- [Form Manager](https://bitbucket.org/AaronPresley/js-helpers/src/master/FormManager/) - A collection of helpers when working with forms at Inavero
- [Notion](https://bitbucket.org/AaronPresley/js-helpers/src/master/Notion/) - A custom-written tooltip written for Inavero
- [Table Manager](https://bitbucket.org/AaronPresley/js-helpers/src/master/TableManager/) - A collection of helpers when working with tables at Inavero
